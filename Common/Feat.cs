﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{

    class Feat
    {

        public string name { get; set; }
        public string prerequisite { get; set; }
        public string[] text { get; set; }
        public Modifier modifier { get; set; }

        public Feat(string nname, string nprerequisite, string[] ntext, Modifier nmodifier)
        {
            name = nname;
            prerequisite = nprerequisite;
            text = ntext;
            modifier = nmodifier;
        }


    }
}
