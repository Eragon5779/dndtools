﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    class Background
    {

        public string name { get; set; }
        public string[] proficiency { get; set; }
        public List<Trait> traits { get; set; }

        public Background(string nname, string[] nproficiency, List<Trait> ntraits)
        {
            name = nname;
            proficiency = nproficiency;
            traits = ntraits;
        }

    }
}
