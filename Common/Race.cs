﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    class Race
    {

        public string name { get; set; }
        public string size { get; set; }
        public string speed { get; set; }
        public string[] ability { get; set; }
        public string[] proficiency { get; set; }
        public List<Trait> traits { get; set; }

        public Race(string nname, string nsize, string nspeed, string[] nability, List<Trait> ntraits, string[] nproficiency = null)
        {
            name = nname;
            size = nsize;
            speed = nspeed;
            ability = nability;
            traits = ntraits;
            proficiency = nproficiency;
        }

    }
}
