﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    class Spell
    {

        public bool concentration { get; set; }
        public string page { get; set; }
        public string range { get; set; }
        public string name { get; set; }
        public string components { get; set; }
        public bool ritual { get; set;}
        public string duration { get; set; }
        public string casting_time { get; set; }
        public int level { get; set; }
        public string school { get; set; }
        public Dictionary<string, bool> _class { get; set;}

        public Spell(bool nconcentration, string npage, string nrange, string nname, string ncomponents, bool nritual, string nduration, string ncasting_time, int nlevel, string nschool, Dictionary<string, bool> n_class)
        {
            concentration = nconcentration;
            page = npage;
            name = nname;
            ritual = nritual;
            _class = n_class;
            casting_time = ncasting_time;
            components = ncomponents;
            duration = nduration;
            level = nlevel;
            range = nrange;
            school = nschool;
        }
    }
}
