﻿using System;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using System.IO;
using System.Net;

namespace Common
{
    class DataManagement
    {
        public static readonly Dictionary<string, string> ItemPropCodes = new Dictionary<string, string>()
        {
            {  "A", "Ammo" },
            {  "F", "Finesse" },
            {  "H", "Heavy" },
            { "2H", "Two-Handed" },
            {  "L", "Light" },
            { "LD", "Loading" },
            {  "R", "Reach" },
            {  "S", "Special" },
            {  "T", "Thrown" },
            {  "V", "Versatile" }
        };
        public static readonly Dictionary<string, string> DamageCodes = new Dictionary<string, string>()
        {
            { "S", "Slashing" },
            { "P", "Piercing" },
            { "B", "Bludgeoning" }
        };
        public static readonly Dictionary<int, int> LevelExp = new Dictionary<int, int>()
        {
            {  1, 0 },
            {  2, 300 },
            {  3, 900 },
            {  4, 2700 },
            {  5, 6500 },
            {  6, 14000 },
            {  7, 23000 },
            {  8, 34000 },
            {  9, 48000 },
            { 10, 64000 },
            { 11, 85000 },
            { 12, 100000 },
            { 13, 120000 },
            { 14, 140000 },
            { 15, 165000 },
            { 16, 195000 },
            { 17, 225000 },
            { 18, 165000 },
            { 19, 305000 },
            { 20, 355000 }
        };
        public static readonly Dictionary<string, string> ItemCodes = new Dictionary<string, string>()
        {
            {  "$", "Sellable" },
            {  "G", "Gear" },
            { "LA", "Light Armor" },
            { "MA", "Medium Armor" },
            { "HA", "Heavy Armor" },
            {  "S", "Shield" },
            {  "A", "Arrow" },
            {  "M", "Melee" },
            {  "R", "Ranged" },
            {  "W", "Wonderous Item" },
            {  "P", "Potion" },
            { "ST", "Staff" },
            { "RD", "Rod" },
            { "WD", "Wand" },
            { "RG", "Ring" },
            { "SC", "Scroll" }
        };
        public string fullPath { get; set; }


        private static readonly string folderPath = @"C:\Users\" + Environment.UserName + @"\Documents\D&D Tools";
        private static readonly string characterPath = folderPath + @"\Characters.json";
        private int threads_left = 0;

        public DataManagement()
        {
            this.fullPath = folderPath + @"\compendium.json";
            retrieve_files();
        }

        public void retrieve_files()
        {
            if (!File.Exists(fullPath))
            {
                threads_left += 1;
                ThreadPool.QueueUserWorkItem(get_file, "compendium.json");
            }
            SpinWait.SpinUntil(() => threads_left == 0);
        }

        private void get_file(object path)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile($"https://gitlab.com/Eragon5779/dndtools/raw/master/Compendiums/{path}", folderPath + $"\\{path}");
            }
            threads_left -= 1;
        }

        public bool checkData()
        {
            bool CharacterFile = false;
            if (Directory.Exists(folderPath))
            {
                if (File.Exists(characterPath))
                {
                    CharacterFile = true;
                }
                else
                {
                    CharacterFile = false;
                }
            }
            else
            {
                Directory.CreateDirectory(folderPath);
                return false;
            }
            return CharacterFile;
        }
        public void writeCharacters(Dictionary<string, Character> characters)
        {
            string jsonString = JsonConvert.SerializeObject(characters, Formatting.Indented);
            checkData();
            File.WriteAllText(characterPath, jsonString);
        }
        public Dictionary<string, Character> readCharacters()
        {
            if (checkData())
            {
                return JsonConvert.DeserializeObject<Dictionary<string, Character>>(File.ReadAllText(characterPath));
            }
            return null;
        }

        public Compendium LoadCompendium()
        {
            return JsonConvert.DeserializeObject<Compendium>(File.ReadAllText(fullPath));
        }
    }
}
