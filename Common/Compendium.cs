﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    class Compendium
    {
        public int version { get; set; }
        public Dictionary<string, Item> items { get; set; }
        public Dictionary<string, Race> races { get; set; }
        public Dictionary<string, Background> backgrounds { get; set; }
        public Dictionary<string, Feat> feats { get; set; }
        public Dictionary<string, Spell> spells { get; set; }
        public Dictionary<string, Class> classes { get; set; }

        public Compendium(Dictionary<string, Item> items, Dictionary<string, Race> races,
                          Dictionary<string, Background> backgrounds, Dictionary<string, Feat> feats,
                          Dictionary<string, Spell> spells, Dictionary<string, Class> classes)
        {
            this.items = items;
            this.races = races;
            this.backgrounds = backgrounds;
            this.feats = feats;
            this.spells = spells;
            this.classes = classes;
        }
    }
}
