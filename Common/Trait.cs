﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Common
{
    class Trait
    {
        
        public string name { get; set; }
        public string text { get; set; }
        public string[] feature { get; set; }

        public Trait(string nname, string ntext = null, string[] nfeature = null)
        {
            name = nname;
            text = ntext;
            feature = nfeature;
        }

    }
}
