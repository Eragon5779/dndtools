﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    class Modifier
    {
        public string category { get; set; }
        public string text { get; set; }

        public Modifier(string category, string text)
        {
            this.category = category;
            this.text = text;
        }
    }

    class Item
    {
        public string name { get; set; }
        public string type { get; set; }
        public string magic { get; set; }
        public string dmg1 { get; set; }
        public string dmg2 { get; set; }
        public string dmgType { get; set; }
        public string property { get; set; }
        public string range { get; set; }
        public string rarity { get; set; }
        public string value { get; set; }
        public string weight { get; set; }
        public string[] text { get; set; }
        public string[] roll { get; set; }
        public Modifier[] modifier { get; set; }
        public bool? stealth { get; set; }
        public string strength { get; set; }
        public string attunement { get; set; }
        public string ac { get; set; }
        public Item(string nname, string ntype, string nmagic, string ndmg1, string ndmg2, string ndmgType, string nproperty,
                    string nrange, string nrarity, string nvalue, string nweight, string[] ntext, string[] nroll, Modifier[] nmodifiers,
                    bool nstealth, string nstrength, string nattunement, string ac)
        {
            name = nname;
            type = ntype;
            magic = nmagic;
            dmg1 = ndmg1;
            dmg2 = ndmg2;
            dmgType = ndmgType;
            property = nproperty;
            range = nrange;
            rarity = nrarity;
            value = nvalue;
            weight = nweight;
            text = ntext;
            roll = nroll;
            modifier = nmodifiers;
            stealth = nstealth;
            strength = nstrength;
            attunement = nattunement;
            this.ac = ac;
        }
    }
}
