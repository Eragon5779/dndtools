﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    class Stat
    {
        public int Score { get; set; }
        public int Modifier { get; set; }
        public bool Saving { get; set; }

        public Stat(int Score, bool Saving, int Modifier)
        {
            this.Score = Score;
            this.Modifier = Modifier;
            this.Saving = Saving;
        }
        public Stat()
        {
            this.Score = 0;
            this.Modifier = 0;
            this.Saving = false;
        }
    }
}
