﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    class Monster
    {
        public string name { get; set; }
        public string size { get; set; }
        public string type { get; set; }
        public string alignment { get; set; }
        public string ac { get; set; }
        public string hp { get; set; }
        public string speed { get; set; }
        public string str { get; set; }
        public string dex { get; set; }
        public string con { get; set; }
        public string intel { get; set; }
        public string wis { get; set; }
        public string cha { get; set; }
        public string skill { get; set; }
        public string passive { get; set; }
        public string languages { get; set; }
        public string cr { get; set; }
        public List<Dictionary<string,string>> trait { get; set; }
        public List<Dictionary<string, string>> action { get; set; }

        public Monster(string name, string size, string type, string alignment, 
                       string ac, string hp, string speed, string dex, string con, 
                       string intel, string wis, string cha, string skill,
                       string passive, string languages, string cr, 
                       List<Dictionary<string, string>> trait, List<Dictionary<string, string>> action)
        {
            this.name = name;
            this.size = size;
            this.alignment = alignment;
            this.ac = ac;
            this.hp = hp;
            this.speed = speed;
            this.dex = dex;
            this.con = con;
            this.intel = intel;
            this.wis = wis;
            this.cha = cha;
            this.skill = skill;
            this.passive = passive;
            this.languages = languages;
            this.cr = cr;
            this.trait = trait;
            this.action = action;
        }
    }
}
