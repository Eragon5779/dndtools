﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    class SpellSlot
    {

        public bool optional {get; set;}
        public string text { get; set; }

        public SpellSlot(bool optional, string text)
        {
            this.optional = optional;
            this.text = text;
        }

    }
}
