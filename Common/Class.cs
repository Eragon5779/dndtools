﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    class Feature
    {
        public string name { get; set; }
        public List<string> text { get; set; }
        public bool optional { get; set; }
        public Feature(string name, List<string> text, bool optional)
        {
            this.name = name;
            this.text = text;
            this.optional = optional;
        }
    }
    class Level
    {
        public string level { get; set; }
        public SpellSlot slots { get; set; }
        public List<Feature> feature { get; set; }
    }
    class Class
    {
        public string name { get; set; }
        public string hd { get; set; }
        public string proficiency { get; set; }
        public string spellAbility { get; set; }
        public Dictionary<string, Level> autolevel { get; set; }
        public string[] skills { get; set; }
        public int maxProfs { get; set; }

        public Class(string nname, string nhd, string nproficiency, string nspellAbility, Dictionary<string, Level> nautolevel, string[] nskills, int nmaxProfs)
        { 
            name = nname;
            hd = nhd;
            proficiency = nproficiency;
            spellAbility = nspellAbility;
            autolevel = nautolevel;
            skills = nskills;
            maxProfs = nmaxProfs;
        }
    }
}
