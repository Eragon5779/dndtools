﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    class Character
    {   
        // Name and Type of Character
        public string Name { get; set; }
        public string Player { get; set; }

        // Stat variables
        public Stat STR { get; set; }
        public Stat DEX { get; set; }
        public Stat CON { get; set; }
        public Stat INT { get; set; }
        public Stat WIS { get; set; }
        public Stat CHA { get; set; }

        // Class and Race based stats
        public int AC { get; set; }
        public string HitDice { get; set; }
        public int maxHP { get; set; }
        public int walkSpeed { get; set; }

        //Character Info
        public string Race { get; set; }
        public string Class { get; set; }
        public string Background { get; set; }
        public int currentHP { get; set; }
        public int Level { get; set; }
        public int profBonus { get; set; }
        public string Status { get; set; }
        public int SpellDC { get; set; }

        //Proficiencies
        public bool[] Proficiencies { get; set; }

        public Character(string nName, string nPlayer, string nRace, string nClass, string nBackground, int nAC, int nmaxHP,
                         string nHitDice, int nwaslkSpeed, int nprofBonus, Stat nSTR = null, Stat nDEX = null, Stat nCON = null, Stat nINT = null, 
                         Stat nWIS = null, Stat nCHA = null, bool[] nProficiencies = null, int ncurrentHP = -1, int nlevel = 1, string nstatus = "")
        {

            Name = nName;
            Player = nPlayer;
            STR = nSTR ?? new Stat();
            DEX = nDEX ?? new Stat();
            CON = nCON ?? new Stat();    
            INT = nINT ?? new Stat();
            WIS = nWIS ?? new Stat();
            CHA = nCHA ?? new Stat();
            AC = nAC;
            walkSpeed = nwaslkSpeed;

            Race = nRace;
            Class = nClass;
            Background = nBackground;
            HitDice = nHitDice;
            maxHP = nmaxHP;
            currentHP = (ncurrentHP == -1) ? maxHP : ncurrentHP;

            Level = nlevel;
            profBonus = nprofBonus;
            Status = nstatus;
            

            Proficiencies = nProficiencies ?? new bool[18];

        }
        
    }
}
