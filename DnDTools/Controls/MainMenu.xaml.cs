﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DnDTools.Controls
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl
    {
        MainWindow parent;
        public MainMenu()
        {
            InitializeComponent();
            parent = (App.Current.MainWindow as MainWindow);
        }

        private void btnCharacters_Click(object sender, RoutedEventArgs e)
        {
            parent.load_character_menu();
        }

        private void btnCompendiums_Click(object sender, RoutedEventArgs e)
        {
            parent.load_compendium_menu();
        }
    }
}
