﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DnDTools.Data;

namespace DnDTools.Controls
{
    /// <summary>
    /// Interaction logic for CharacterView.xaml
    /// </summary>
    public partial class CharacterView : UserControl
    {
        #region Utility Vars
        public readonly string[] Alignments = new string[] { "Neutral", "Neutral Good", "Neutral Evil",
                                                             "Chaotic Neutral", "Chaotic Good", "Chaotic Evil",
                                                             "Lawful Neutral", "Lawaful Good", "Lawful Evil"};
        public bool Editing { get; set; }
        private MainWindow parent;
        private Handler handler = Handler.GetHandler();
        public List<int> Range20 { get; set; }
        #endregion
        #region Character Info
        public string CharacterName { get; set; }
        public string PlayerName { get; set; }
        private Character CurrentCharacter;
        private Class CurrentClass;
        private Race CurrentRace;
        public int SelectedLevel { get; set; }
        public string SelectedClass { get; set; }
        public string SelectedBackground { get; set; }
        public string SelectedRace { get; set; }
        public string Experience { get; set; }
        public string SelectedAlignment { get; set; }
        public int ProfBonus { get; set; }
        #endregion
        #region Ability Scores
        public int SelectedStr { get; set; }
        public int SelectedDex { get; set; }
        public int SelectedCon { get; set; }
        public int SelectedInt { get; set; }
        public int SelectedWis { get; set; }
        public int SelectedCha { get; set; }
        #endregion
        #region Health and Defense
        public int ArmorClass { get; set; }
        public int Initiative { get; set; }
        public int MaxHP { get; set; }
        public int CurrentHP { get; set; }
        public int TemporaryHP { get; set; }
        public int HitDie { get; set; }
        public int Inspiration { get; set; }
        /*
         * Immunities
         * Resistances
         * Inspiration
         * Armors
         * Defensive Spells
         * Passives
         * Conditions
         */
        #endregion
        #region Equipment
        /*
        * Different categories (scrollviews)
        * - Armor
        * - Weapons
        * - Tools
        * - Potions
        * - Other
        */
        #endregion
        public CharacterView()
        {
            Range20 = Enumerable.Range(1, 20).ToList(); // Sets a list from 1-20
            if (!string.IsNullOrEmpty(CharacterName))
            {
                handler.characters.TryGetValue(CharacterName, out CurrentCharacter);
            }
            if (CurrentCharacter == null)
            {
                // Set default values
                SelectedLevel = 1;
                ProfBonus = 2;
                Editing = true;
                SelectedAlignment = "Neutral";
                CharacterName = "";
                PlayerName = "";
                Experience = "0";
                SelectedStr = 10;
                SelectedDex = 10;
                SelectedCon = 10;
                SelectedInt = 10;
                SelectedWis = 10;
                SelectedCha = 10;
            }
            else
            {
                // Load character data
                CharacterName = CurrentCharacter.Name;
                PlayerName = CurrentCharacter.Player;
                SelectedClass = CurrentCharacter.Class;
                SelectedBackground = CurrentCharacter.Background;
                SelectedRace = CurrentCharacter.Race;
                SelectedLevel = CurrentCharacter.Level;
                //SelectedAlignment = current.Alignment;
                //SelectedExp = current.Exp;
            }
            parent = (App.Current.MainWindow as MainWindow);
            InitializeComponent();
            ConfigureBindings();
        }

        private void ConfigureBindings()
        {
            cbLevel.ItemsSource = Range20;
            cbLevel.SelectedValue = SelectedLevel;
            cbClass.ItemsSource = handler.compendium.classes.Keys.ToList();
            cbClass.SelectedValue = SelectedClass;
            cbRace.ItemsSource = handler.compendium.races.Keys.ToList();
            cbRace.SelectedValue = SelectedRace;
            cbBackground.ItemsSource = handler.compendium.backgrounds.Keys.ToList();
            cbBackground.SelectedValue = SelectedBackground;
            txtCharName.SelectedText = CharacterName;
            txtPlayerName.SelectedText = PlayerName;
            txtExp.SelectedText = Experience;
            cbAlignment.ItemsSource = Alignments;
            cbAlignment.SelectedValue = SelectedAlignment;

            // Abilities
            cbStr.ItemsSource = Range20;
            cbStr.SelectedValue = SelectedStr;
            lblStrMod.Content = Convert.ToString(GetMod(SelectedStr));
            cbDex.ItemsSource = Range20;
            cbDex.SelectedValue = SelectedDex;
            lblDexMod.Content = Convert.ToString(GetMod(SelectedDex));
            cbCon.ItemsSource = Range20;
            cbCon.SelectedValue = SelectedCon;
            lblConMod.Content = Convert.ToString(GetMod(SelectedCon));
            cbInt.ItemsSource = Range20;
            cbInt.SelectedValue = SelectedInt;
            lblIntMod.Content = Convert.ToString(GetMod(SelectedInt));
            cbWis.ItemsSource = Range20;
            cbWis.SelectedValue = SelectedWis;
            lblWisMod.Content = Convert.ToString(GetMod(SelectedWis));
            cbCha.ItemsSource = Range20;
            cbCha.SelectedValue = SelectedCha;
            lblChaMod.Content = Convert.ToString(GetMod(SelectedCha));
        }

        private int GetMod(int ability)
        {
            return Convert.ToInt32(Math.Floor((ability - 10) / 2.0));
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            parent.Dispatcher.Invoke(() =>
            {
                parent.gMain.Children[1].Visibility = Visibility.Visible;
            });
            Visibility = Visibility.Collapsed;
            CharacterName = null;
        }

        #region Combo Boxes
        private void CbStr_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedStr = Convert.ToInt32((sender as ComboBox).SelectedValue);
            lblStrMod.Content = $"{CheckPositive(SelectedStr)}{GetMod(SelectedStr)}";
            lblStrSav.Content = (chkStrProf.IsChecked == true) ? 
                $"{CheckPositive(SelectedStr)}{GetMod(SelectedStr) + ProfBonus}" : 
                $"{CheckPositive(SelectedStr)}{GetMod(SelectedStr)}";
            UpdateStr();
        }
        private void CbDex_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedDex = Convert.ToInt32((sender as ComboBox).SelectedValue);
            lblDexMod.Content = $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex)}";
            lblDexSav.Content = (chkDexProf.IsChecked == true) ? 
                $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex) + ProfBonus}" : 
                $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex)}";
            UpdateDex();
        }
        private void CbCon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedCon = Convert.ToInt32((sender as ComboBox).SelectedValue);
            lblConMod.Content = $"{CheckPositive(SelectedCon)}{GetMod(SelectedCon)}";
            lblConSav.Content = (chkConProf.IsChecked == true) ? 
                $"{CheckPositive(SelectedCon)}{GetMod(SelectedCon) + ProfBonus}" : 
                $"{CheckPositive(SelectedCon)}{GetMod(SelectedCon)}";
            UpdateCon();
        }
        private void CbInt_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedInt = Convert.ToInt32((sender as ComboBox).SelectedValue);
            lblIntMod.Content = $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt)}";
            lblIntSav.Content = (chkIntProf.IsChecked == true) ? 
                $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt) + ProfBonus}" : 
                $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt)}";
            UpdateInt();
        }
        private void CbWis_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedWis = Convert.ToInt32((sender as ComboBox).SelectedValue);
            lblWisMod.Content = $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis)}";
            lblWisSav.Content = (chkWisProf.IsChecked == true) ? 
                $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis) + ProfBonus}" : 
                $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis)}";
            UpdateWis();
        }
        private void CbCha_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedCha = Convert.ToInt32((sender as ComboBox).SelectedValue);
            lblChaMod.Content = $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha)}";
            lblChaSav.Content = (chkChaProf.IsChecked == true) ?
                $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha) + ProfBonus}" :
                $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha)}";
            UpdateCha();
        }
        private void CbRace_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedRace = (sender as ComboBox).SelectedValue as string;
            CurrentRace = handler.compendium.races[SelectedRace];
            lblWalkSpd.Content = $"{CurrentRace.speed} ft.";
        }
        private void CbLevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int OldLevel = SelectedLevel;
            SelectedLevel = Convert.ToInt32((sender as ComboBox).SelectedValue);
            ProfBonus = Convert.ToInt32(Math.Floor((SelectedLevel - 1) / 4.0)) + 2;
            lblProf.Content = $"+{ProfBonus}";
            if (SelectedLevel != OldLevel)
            {
                Experience = $"{DataManagement.LevelExp[SelectedLevel]}";
                txtExp.Text = Experience;
            }
            UpdateAll();
        }
        private void TxtExp_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !char.IsDigit(Handler.GetCharFromKey(e.Key));
        }

        private void CbClass_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedClass = (sender as ComboBox).SelectedValue as string;
            CurrentClass = handler.compendium.classes[SelectedClass];
            string[] profs = CurrentClass.proficiency.Split(',');
            // Save proficiencies
            chkChaProf.IsChecked = profs.Contains("Charisma");
            chkDexProf.IsChecked = profs.Contains("Dexterity");
            chkStrProf.IsChecked = profs.Contains("Strength");
            chkWisProf.IsChecked = profs.Contains("Wisdom");
            chkIntProf.IsChecked = profs.Contains("Intelligence");
            chkConProf.IsChecked = profs.Contains("Constitution");
            // Skill proficiencies
            chkAcroProf.IsChecked = CurrentClass.skills.Contains("Acrobatics");
            chkAnimalProf.IsChecked = CurrentClass.skills.Contains("Animal Handling");
            chkArcanaProf.IsChecked = CurrentClass.skills.Contains("Arcana");
            chkAthleticsProf.IsChecked = CurrentClass.skills.Contains("Athletics");
            chkDeceptionProf.IsChecked = CurrentClass.skills.Contains("Deception");
            chkHistoryProf.IsChecked = CurrentClass.skills.Contains("History");
            chkInsightProf.IsChecked = CurrentClass.skills.Contains("Insight");
            chkIntimProf.IsChecked = CurrentClass.skills.Contains("Intimidation");
            chkInvestProf.IsChecked = CurrentClass.skills.Contains("Investigation");
            chkMedicineProf.IsChecked = CurrentClass.skills.Contains("Medicine");
            chkNatureProf.IsChecked = CurrentClass.skills.Contains("Nature");
            chkPerceptionProf.IsChecked = CurrentClass.skills.Contains("Perception");
            chkPerformanceProf.IsChecked = CurrentClass.skills.Contains("Performance");
            chkPersuasionProf.IsChecked = CurrentClass.skills.Contains("Persuasion");
            chkReligionProf.IsChecked = CurrentClass.skills.Contains("Religion");
            chkSleightProf.IsChecked = CurrentClass.skills.Contains("Sleight of Hand");
            chkStealthProf.IsChecked = CurrentClass.skills.Contains("Stealth");
            chkSurvivalProf.IsChecked = CurrentClass.skills.Contains("Survival");

            UpdateAll();
        }

        #endregion
        #region Updaters
        void UpdateAll()
        {
            UpdateCha();
            UpdateWis();
            UpdateInt();
            UpdateDex();
            UpdateStr();
            UpdateCon();
        }

        private void UpdateCha()
        {
            lblChaSav.Content = (chkChaProf.IsChecked == true) ? $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha) + ProfBonus}" : $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha)}";

            lblPerformanceMod.Content = (chkPerformanceProf.IsChecked == true) ? $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha) + ProfBonus}" : $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha)}";
            lblPersuasionMod.Content = (chkPersuasionProf.IsChecked == true) ? $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha) + ProfBonus}" : $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha)}";
            lblDeceptionMod.Content = (chkDeceptionProf.IsChecked == true) ? $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha) + ProfBonus}" : $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha)}";
            lblIntimMod.Content = (chkIntimProf.IsChecked == true) ? $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha) + ProfBonus}" : $"{CheckPositive(SelectedCha)}{GetMod(SelectedCha)}";
        }

        private void UpdateWis()
        {
            lblWisSav.Content = (chkWisProf.IsChecked == true) ? $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis) + ProfBonus}" : $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis)}";

            lblAnimalMod.Content = (chkAnimalProf.IsChecked == true) ? $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis) + ProfBonus}" : $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis)}";
            lblInsightMod.Content = (chkInsightProf.IsChecked == true) ? $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis) + ProfBonus}" : $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis)}";
            lblMedicineMod.Content = (chkMedicineProf.IsChecked == true) ? $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis) + ProfBonus}" : $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis)}";
            lblPerceptionMod.Content = (chkPerceptionProf.IsChecked == true) ? $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis) + ProfBonus}" : $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis)}";
            lblSurvivalMod.Content = (chkSurvivalProf.IsChecked == true) ? $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis) + ProfBonus}" : $"{CheckPositive(SelectedWis)}{GetMod(SelectedWis)}";
        }

        private void UpdateInt()
        {
            lblIntSav.Content = (chkIntProf.IsChecked == true) ? $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt) + ProfBonus}" : $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt)}";

            lblArcanaMod.Content = (chkArcanaProf.IsChecked == true) ? $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt) + ProfBonus}" : $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt)}";
            lblHistoryMod.Content = (chkHistoryProf.IsChecked == true) ? $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt) + ProfBonus}" : $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt)}";
            lblInvestMod.Content = (chkInvestProf.IsChecked == true) ? $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt) + ProfBonus}" : $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt)}";
            lblNatureMod.Content = (chkNatureProf.IsChecked == true) ? $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt) + ProfBonus}" : $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt)}";
            lblReligionMod.Content = (chkReligionProf.IsChecked == true) ? $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt) + ProfBonus}" : $"{CheckPositive(SelectedInt)}{GetMod(SelectedInt)}";
        }

        private void UpdateCon()
        {
            lblConSav.Content = (chkConProf.IsChecked == true) ? $"{CheckPositive(SelectedCon)}{GetMod(SelectedCon) + ProfBonus}" : $"{CheckPositive(SelectedCon)}{GetMod(SelectedCon)}";
        }

        private void UpdateStr()
        {
            lblStrSav.Content = (chkStrProf.IsChecked == true) ? $"{CheckPositive(SelectedStr)}{GetMod(SelectedStr) + ProfBonus}" : $"{CheckPositive(SelectedStr)}{GetMod(SelectedStr)}";

            lblAthleticsMod.Content = (chkAthleticsProf.IsChecked == true) ? $"{CheckPositive(SelectedStr)}{GetMod(SelectedStr) + ProfBonus}" : $"{CheckPositive(SelectedStr)}{GetMod(SelectedStr)}";
        }

        private void UpdateDex()
        {
            lblDexSav.Content = (chkDexProf.IsChecked == true) ? $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex) + ProfBonus}" : $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex)}";

            lblAcroMod.Content = (chkAcroProf.IsChecked == true) ? $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex) + ProfBonus}" : $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex)}";
            lblSleightMod.Content = (chkSleightProf.IsChecked == true) ? $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex) + ProfBonus}" : $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex)}";
            lblStealthMod.Content = (chkStealthProf.IsChecked == true) ? $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex) + ProfBonus}" : $"{CheckPositive(SelectedDex)}{GetMod(SelectedDex)}";
        }

        private string CheckPositive(int stat)
        {
            return (GetMod(stat) >= 0) ? "+" : "";
        }
        #endregion
    }
}
