﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DnDTools.Controls
{
    /// <summary>
    /// Interaction logic for CompendiumList.xaml
    /// </summary>
    public partial class CompendiumList : UserControl
    {
        MainWindow parent;
        public CompendiumList()
        {
            parent = (App.Current.MainWindow as MainWindow);
            InitializeComponent();
        }

        private void BtnItems_Click(object sender, RoutedEventArgs e)
        {
            parent.load_item_view();
        }
    }
}
