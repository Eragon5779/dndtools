﻿using DnDTools.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Common;
using System.Globalization;

namespace DnDTools.Controls.Compendiums
{
    /// <summary>
    /// Interaction logic for ItemView.xaml
    /// </summary>
    public partial class ItemView : UserControl
    {
        private TextInfo tc = new CultureInfo("en-US", false).TextInfo;
        private List<string> rarities = new List<string>() { "Common", "Uncommon", "Rare", "Very Rare", "Legendary", "Artifact" };
        private List<string> BoolToString = new List<string>() { "Yes", "No" };
        private Item current = null;
        private Handler handler = Handler.GetHandler();
        public ItemView()
        {
            InitializeComponent();
            acbSearchItem.ItemsSource = handler.compendium.items.Keys.ToList();
            cmbItemType.ItemsSource = DataManagement.ItemCodes.Values.ToList();
            cmbDmg.ItemsSource = DataManagement.DamageCodes.Values.ToList();
            cmbRarity.ItemsSource = rarities;
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void AcbSearchItem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (acbSearchItem.SelectedItem != null)
            {
                current = handler.compendium.items[acbSearchItem.SelectedItem as string];
                lblName.Content = acbSearchItem.SelectedItem as string;
                string desc = "";
                foreach (string part in current.text)
                {
                    if (!part.Contains("Rarity") && !part.Contains("Source") &&
                        !part.ToLower().Contains("attunement"))
                    {
                        desc += part + "\n";
                    }
                }
                txtDesc.Text = desc.Substring(0, desc.Count() - 1);
                cmbItemType.SelectedItem = DataManagement.ItemCodes[current.type];
                txtValue.Text = current.value ?? "Priceless";
                txtWeight.Text = current.weight;
                if (!string.IsNullOrEmpty(current.rarity))
                {
                    cmbRarity.SelectedItem = tc.ToTitleCase(current.rarity);
                }
                else
                {
                    cmbRarity.SelectedItem = null;
                }
                txtAttunement.Text = current.attunement ?? "No";
                if (current.dmgType != null)
                {
                    cmbDmg.SelectedItem = DataManagement.DamageCodes[current.dmgType];
                }
                else
                {
                    cmbDmg.SelectedItem = null;
                }
                txtMagic.Text = (current.magic != null) ? "Yes" : "No";
                txtDmgDie1.Text = current.dmg1;
                txtDmgDie2.Text = current.dmg2;
                txtRange.Text = (current.range != null) ? current.range.Replace("/", " / ") : null;
                txtAC.Text = current.ac;
                txtStrength.Text = current.strength;
                txtStealth.Text = (current.stealth == true) ? "Disadvantage" : "Normal";
                string prop = "";
                string proptip = "";
                if (!string.IsNullOrEmpty(current.property))
                {
                    foreach (string p in current.property.Split(','))
                    {
                        proptip += DataManagement.ItemPropCodes[p] + ",";
                    }
                    proptip = proptip.Substring(0, proptip.Count() - 1);
                    prop = proptip;
                }
                crdProperties.ToolTip = string.IsNullOrEmpty(proptip) ? "No Properties" : proptip;
                txtProperties.Text = prop;
                string mods = "";
                if (current.modifier != null)
                {
                    foreach (Modifier m in current.modifier)
                    {
                        mods += tc.ToTitleCase(m.text) + "\n";
                    }
                    mods = mods.Substring(0, mods.Count() - 1);
                }
                txtModifiers.Text = mods;
            }
        }
    }
}
