﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DnDTools.Data;

namespace DnDTools.Controls
{
    /// <summary>
    /// Interaction logic for CharacterList.xaml
    /// </summary>
    public partial class CharacterList : UserControl
    {
        private Handler handler = Handler.GetHandler();
        private List<string> CharNames;
        private List<Button> CharButtons = new List<Button>();
        MainWindow parent;
        public CharacterList()
        {
            parent = (App.Current.MainWindow as MainWindow);
            refresh_names();
            refresh_buttons();
            InitializeComponent();
        }

        private Button craft_button(string name)
        {
            /*
            <Button Content="Characters" Margin="0,10,0,0" FontSize="25"
                Height="{Binding Path=ActualHeight, ElementName=spMenu, Converter={StaticResource mc}, ConverterParameter=@VALUE/8}"
                Width="{Binding Path=ActualWidth, ElementName=spMenu, Converter={StaticResource mc}, ConverterParameter=@VALUE/2}"/>
            */
            Button b = new Button();
            b.Content = name;
            b.HorizontalContentAlignment = HorizontalAlignment.Center;
            b.VerticalContentAlignment = VerticalAlignment.Center;
            Binding widthBinding = new Binding("ActualWidth");
            widthBinding.Source = scvCharacters.Width;
            widthBinding.Converter = new MathConverter();
            widthBinding.ConverterParameter = "@VALUE*.5";
            Binding heightBinding = new Binding("ActualHeight");
            heightBinding.Source = scvCharacters.Width;
            heightBinding.Converter = new MathConverter();
            heightBinding.ConverterParameter = "@VALUE*.2";
            b.FontSize = 20;

            b.SetBinding(WidthProperty, widthBinding);
            b.SetBinding(HeightProperty, heightBinding);
            return b;
        }

        private void refresh_buttons()
        {
            CharButtons = new List<Button>();
            foreach (string c in CharNames)
            {
                CharButtons.Add(craft_button(c));
            }
            foreach (Button b in CharButtons)
            {
                spCharacters.Children.Add(b);
            }
        }

        private void refresh_names()
        {
            if (handler.characters != null)
            {
                CharNames = handler.characters.Keys.ToList();
            }
            else
            {
                CharNames = new List<string>();
            }
        }

        private void BtnAddCharacter_Click(object sender, RoutedEventArgs e)
        {
            parent.load_charater_view();
        }
    }
}
