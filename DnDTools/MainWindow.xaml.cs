﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DnDTools.Controls;
using DnDTools.Controls.Compendiums;

namespace DnDTools
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public enum INDEXES
        {
            MAIN_MENU,
            CHARACTER_MENU,
            CHARACTER_VIEW,
            COMPENDIUM_MENU,
            ITEM_COMPENDIUM
        }
        public MainWindow()
        {
            InitializeComponent();
            // Add all of the elements
            gMain.Children.Add(new MainMenu());
            gMain.Children.Add(new CharacterList());
            gMain.Children.Add(new CharacterView());
            gMain.Children.Add(new CompendiumList());
            gMain.Children.Add(new ItemView());
            // Set all of the element visibilities
            foreach (int i in (int[]) Enum.GetValues(typeof(INDEXES)))
            {
                if (i != 0)
                {
                    gMain.Children[i].Visibility = Visibility.Collapsed;
                }
            }
        }

        public void load_character_menu()
        {
            gMain.Children[(int)INDEXES.MAIN_MENU].Visibility = Visibility.Collapsed;
            gMain.Children[(int)INDEXES.CHARACTER_MENU].Visibility = Visibility.Visible;
        }

        public void load_compendium_menu()
        {
            gMain.Children[(int)INDEXES.MAIN_MENU].Visibility = Visibility.Collapsed;
            gMain.Children[(int)INDEXES.COMPENDIUM_MENU].Visibility = Visibility.Visible;
        }

        public void load_charater_view(string name = null)
        {
            (gMain.Children[(int)INDEXES.CHARACTER_VIEW] as CharacterView).CharacterName = name;
            gMain.Children[(int)INDEXES.MAIN_MENU].Visibility = Visibility.Collapsed;
            gMain.Children[(int)INDEXES.CHARACTER_VIEW].Visibility = Visibility.Visible;
        }
        public void load_item_view()
        {
            gMain.Children[(int)INDEXES.COMPENDIUM_MENU].Visibility = Visibility.Collapsed;
            gMain.Children[(int)INDEXES.ITEM_COMPENDIUM].Visibility = Visibility.Visible;
        }
    }
}
